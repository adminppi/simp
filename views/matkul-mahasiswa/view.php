<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\MatkulMahasiswa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Matkul Mahasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary matkul-mahasiswa-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail MatkulMahasiswa <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'id_matkul',
                'value'=>function($data) {
                    return $data->id_matkul;
                },
            ],
            [
                'attribute'=>'id_mahasiswa',
                'value'=>function($data) {
                    return $data->id_mahasiswa;
                },
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>
        
    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Yakin Akan Menghapus Data?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>
