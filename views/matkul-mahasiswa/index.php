<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MatkulMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Matkul Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary matkul-mahasiswa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Matkul Mahasiswa', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            'id',
            [
                'attribute'=>'id_matkul',
                'value'=>function($data) {
                    return $data->id_matkul;
                },
            ],
            [
                'attribute'=>'id_mahasiswa',
                'value'=>function($data) {
                    return $data->id_mahasiswa;
                },
            ],
            'created_at',
            'updated_at',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
