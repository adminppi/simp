<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Yourtable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary yourtable-form">
    <div class="box-header with-border">
        <h3 class="box-title">Form yourtable</h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin([            
                        'layout'=>'horizontal',
                        'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => '',
                    ],
                    ]]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'startDate')->widget(DatePicker::className(),[
        'pluginOptions'=>['format'=>'yyyy/mm/dd']
    ]) ?>

    <?= $form->field($model, 'endDate')->widget(DatePicker::className(),[
        'pluginOptions'=>['format'=>'yyyy/mm/dd']
    ]) ?>

    <?= $form->field($model, 'units')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    </div>
    <div class="box-footer with-border form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
