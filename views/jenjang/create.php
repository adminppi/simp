<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jenjang */

$this->title = 'Tambah Jenjang';
$this->params['breadcrumbs'][] = ['label' => 'Jenjang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenjang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
