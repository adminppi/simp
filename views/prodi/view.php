<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Prodi */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Prodi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary prodi-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Prodi <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_jenjang',
                'value'=>function($data) {
                    return $data->jenjang->nama;
                },
            ],
            'waktu_dibuat:datetime',
            'waktu_disunting:datetime',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Yakin Akan Menghapus Data?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Daftar Prodi', ['index'], ['class' => 'btn btn-warning btn-flat']); ?>
        </p>
    </div>

</div>
