<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matkul */

$this->title = 'Sunting Matkul';
$this->params['breadcrumbs'][] = ['label' => 'Matkul', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="matkul-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
