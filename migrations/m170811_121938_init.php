<?php

use yii\db\Migration;

class m170811_121938_init extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%dosen}}', [
            'id' => $this->primaryKey(),
            'nip' => $this->string(50),
            'nama' => $this->string(255)->notNull(),
            'tanggal_lahir' => $this->integer()->notNull(),
            'jenis_kelamin' => $this->smallInteger()->notNull(),
            'foto' => $this->string(128),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%jenjang}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%prodi}}', [
            'id' => $this->primaryKey(),
            'id_jenjang' => $this->integer()->notNull(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        /*
        $this->createTable('{{%kelas}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255)->notNull(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);
        */

        $this->createTable('{{%mahasiswa}}', [
            'id' => $this->primaryKey(),
            'id_prodi' => $this->integer()->notNull(),
            // TODO KELAS
            // 'id_kelas' => $this->integer()->notNull(),
            'tahun_ajaran' => $this->string(30)->notNull(),
            'nama' => $this->string(255),
            'jenis_kelamin' => $this->smallInteger()->notNull(),
            'tanggal_lahir' => $this->integer()->notNull(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%matkul_induk}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255)->notNull(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%matkul}}', [
            'id' => $this->primaryKey(),
            'id_matkul_induk' => $this->integer()->notNull(),
            'id_dosen' => $this->integer()->notNull(),
            'semester' => $this->smallInteger()->notNull(),
            'tahun' => $this->integer(5)->notNull(),
            'sks' => $this->smallInteger(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%matkul_mahasiswa}}', [
            'id' => $this->primaryKey(),
            'id_matkul' => $this->integer()->notNull(),
            'id_mahasiswa' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_id_jenjang_prodi', '{{%prodi}}', 'id_jenjang', false);
        $this->createIndex('idx_id_prodi_mahasiswa', '{{%mahasiswa}}', 'id_prodi', false);
        // TODO $this->createIndex('idx_id_kelas_mahasiswa', '{{%mahasiswa}}', 'id_kelas', false);
        $this->createIndex('idx_id_matkul_induk', '{{%matkul}}', 'id_matkul_induk', false);
        $this->createIndex('idx_id_dosen', '{{%matkul}}', 'id_dosen', false);
        $this->createIndex('idx_id_matkul', '{{%matkul_mahasiswa}}', 'id_matkul', false);
        $this->createIndex('idx_id_mahasiswa', '{{%matkul_mahasiswa}}', 'id_mahasiswa', false);


        $this->addForeignKey('fk-prodi-id_jenjang-jenjang-id', '{{%prodi}}', 'id_jenjang', '{{%jenjang}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-mahasiswa-id_prodi-prodi-id', '{{%mahasiswa}}', 'id_prodi', '{{%prodi}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-matkul-id_matkul_induk-matkul-induk-id', '{{%matkul}}', 'id_matkul_induk', '{{%matkul_induk}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-matkul-id_dosen-dosen-id', '{{%matkul}}', 'id_dosen', '{{%dosen}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-matkul-mahasiswa-id_matkul-matkul-id', '{{%matkul_mahasiswa}}', 'id_matkul', '{{%matkul}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-matkul-mahasiswa-id_mahasiswa-mahasiswa-id', '{{%matkul_mahasiswa}}', 'id_mahasiswa', '{{%mahasiswa}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%matkul_mahasiswa}}');
        $this->dropTable('{{%matkul}}');
        $this->dropTable('{{%matkul_induk}}');
        $this->dropTable('{{%mahasiswa}}');
        $this->dropTable('{{%prodi}}');
        $this->dropTable('{{%jenjang}}');
        $this->dropTable('{{%dosen}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
