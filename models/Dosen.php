<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dosen".
 *
 * @property integer $id
 * @property string $nip
 * @property string $nama
 * @property integer $tanggal_lahir
 * @property integer $jenis_kelamin
 * @property string $foto
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Matkul[] $matkuls
 */
class Dosen extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'tanggal_lahir', 'jenis_kelamin'], 'required'],
            [['tanggal_lahir', 'jenis_kelamin', 'waktu_dibuat', 'waktu_disunting'], 'integer'],
            [['nip'], 'string', 'max' => 50],
            [['nama'], 'string', 'max' => 255],
            [['foto'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nip' => 'Nip',
            'nama' => 'Nama',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'foto' => 'Foto',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkuls()
    {
        return $this->hasMany(Matkul::className(), ['id_dosen' => 'id']);
    }

    public function getNamaJenisKelamin()
    {
        if ($this->jenis_kelamin === Mahasiswa::PRIA) {
            return 'Laki-laki';
        } elseif ($this->jenis_kelamin === Mahasiswa::WANITA) {
            return 'Perempuan';
        }
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
