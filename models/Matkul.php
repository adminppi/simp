<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "matkul".
 *
 * @property integer $id
 * @property integer $id_matkul_induk
 * @property integer $id_dosen
 * @property integer $semester
 * @property integer $tahun
 * @property integer $sks
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Dosen $idDosen
 * @property MatkulInduk $idMatkulInduk
 * @property MatkulMahasiswa[] $matkulMahasiswas
 */
class Matkul extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matkul';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_matkul_induk', 'id_dosen', 'semester', 'tahun'], 'required'],
            [['id_matkul_induk', 'id_dosen', 'semester', 'sks', 'waktu_dibuat', 'waktu_disunting'], 'integer'],
            ['tahun', 'integer', 'min' => 1995, 'max' => 2999],
            [['id_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen' => 'id']],
            [['id_matkul_induk'], 'exist', 'skipOnError' => true, 'targetClass' => MatkulInduk::className(), 'targetAttribute' => ['id_matkul_induk' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_matkul_induk' => 'Matkul Induk',
            'id_dosen' => 'Dosen',
            'semester' => 'Semester',
            'tahun' => 'Tahun',
            'sks' => 'Sks',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id' => 'id_dosen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkulInduk()
    {
        return $this->hasOne(MatkulInduk::className(), ['id' => 'id_matkul_induk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkulMahasiswas()
    {
        return $this->hasMany(MatkulMahasiswa::className(), ['id_matkul' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
