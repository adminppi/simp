<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yourtable".
 *
 * @property string $name
 * @property string $startDate
 * @property string $endDate
 * @property integer $units
 * @property integer $price
 */
class Yourtable extends \yii\db\ActiveRecord
{

    public $january;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yourtable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'startDate', 'endDate', 'units', 'price'], 'required'],
            [['startDate', 'endDate'], 'safe'],
            [['units', 'price'], 'integer'],
            [['january'],'double'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'units' => 'Units',
            'price' => 'Price',
        ];
    }
}
